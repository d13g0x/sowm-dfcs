#ifndef CONFIG_H
#define CONFIG_H

#define MOD Mod4Mask
#define BORDER_COLOR "#b9b9b9"
#define BORDER_WIDTH 3

const char* menu[]    = {"dmenu_run",      0};
const char* termcmd[]  = { "urxvtc", "-pe", "-tabbedalt", NULL };                 
const char* sscmd[] = { "dwm-screenshot", "-s", NULL };                           
const char* ssscmd[] = { "dwm-screenshot", "-m", NULL };                          
const char* n3cmd[] = { "tabn3", NULL };                                          
const char* brightup[] = { "brightness", "+", NULL };                             
const char* brightdw[] = { "brightness", "-", NULL };                             
const char* pocmd[] = { "menu-apagar", NULL };                                    
const char* quitcmd[] = {"pkill",  "sowm",  0};
const char* ltiling[] = {"wm-tl", "-l", 0};
const char* rtiling[] = {"wm-tl", "-r", 0};
const char* ttiling[] = {"wm-tl", "-t", 0};
const char* btiling[] = {"wm-tl", "-b", 0};
const char* otiling[] = {"wm-tl", "-o", 0};


static struct key keys[] = {
    {MOD,      	        XK_q,   win_kill,   {0}},
    {MOD,      		XK_c,   win_center, {0}},
    {MOD,      		XK_f,   win_fs,     {0}},
    {MOD|ShiftMask,	XK_q,	    run, {.com = quitcmd}},
    {MOD,		XK_Up,	    run, {.com = ttiling}},
    {MOD,		XK_Down,    run, {.com = btiling}},
    {MOD,		XK_Right,   run, {.com = rtiling}},
    {MOD,		XK_Left,    run, {.com = ltiling}},
    {MOD,		XK_g,       run, {.com = otiling}},

    {Mod1Mask,           XK_Tab, win_next,   {0}},
    {Mod1Mask|ShiftMask, XK_Tab, win_prev,   {0}},

    {MOD, XK_p,      run, {.com = menu}},
    {MOD, XK_Return, run, {.com = termcmd}},
    {ControlMask, XK_Print,       run, {.com = ssscmd}},     
    {ControlMask|ShiftMask, XK_n, run, {.com = n3cmd}},     
    {0,   XK_Print,  		  run, {.com = sscmd}},     
    {0,   XF86XK_MonBrightnessUp, run, {.com = brightup}},
    {0,   XF86XK_MonBrightnessDown, run, {.com = brightdw}},
    {0,   XF86XK_PowerOff,        run, {.com = pocmd}}, 


    {MOD,           XK_1, ws_go,     {.i = 1}},
    {MOD|ShiftMask, XK_1, win_to_ws, {.i = 1}},
    {MOD,           XK_2, ws_go,     {.i = 2}},
    {MOD|ShiftMask, XK_2, win_to_ws, {.i = 2}},
    {MOD,           XK_3, ws_go,     {.i = 3}},
    {MOD|ShiftMask, XK_3, win_to_ws, {.i = 3}},
    {MOD,           XK_4, ws_go,     {.i = 4}},
    {MOD|ShiftMask, XK_4, win_to_ws, {.i = 4}},
    {MOD,           XK_5, ws_go,     {.i = 5}},
    {MOD|ShiftMask, XK_5, win_to_ws, {.i = 5}},
    {MOD,           XK_6, ws_go,     {.i = 6}},
    {MOD|ShiftMask, XK_6, win_to_ws, {.i = 6}},
};

#endif
